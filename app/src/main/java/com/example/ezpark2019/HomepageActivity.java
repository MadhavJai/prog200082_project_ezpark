package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.example.ezpark2019.ui.AppManualFragment;
import com.example.ezpark2019.ui.SupportFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomepageActivity extends AppCompatActivity {

    String[] homepageOptions = new String[] {

           "Add Parking", "View Parking receipt", "View parking receipt list", "Search nearby parking facilities", "Update user profile", "Parking app manual",
            "Customer support", "Preferences", "Sign out"
    };

    ListView homePageListview;
    ArrayAdapter<String> listViewAdapter;
    ArrayList<String> listViewOptions = new ArrayList<String>(Arrays.asList(homepageOptions));

    private FrameLayout fragmentContainer;

    String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        email = this.getIntent().getStringExtra("EXTRA_EMAIL");

        fragmentContainer = findViewById(R.id.fragment_container);

        homePageListview = findViewById(R.id.lstHomepage);
        listViewAdapter= new ArrayAdapter<String>( this, R.layout.homepage_option_item, R.id.txtHomepageOption, listViewOptions);

        homePageListview.setAdapter(listViewAdapter);

        listViewListener();

    }

//    @Override
//    public void onBackPressed() {
//
//        changeToSignOutActivity();
//        super.onBackPressed();
//    }


    private void listViewListener() {

       homePageListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String title = listViewOptions.get(i);

                switch (listViewOptions.get(i)){

                    case "Add Parking":
                        Intent intentObject = new Intent(HomepageActivity.this, NewParkingActivity.class);
                        startActivity(intentObject);
                        finish();
                        break;
                    case "View Parking receipt":
                        //intent to view parking receipts activity
                        this.openParkingReceipt();
                        break;
                    case "View parking receipt list":
                        //intent for history of receipts activity
                        this.openParkingReceiptList();
                        break;
                    case "Search nearby parking facilities":
                        //intent for search parking activity
                        openMapsActivity();
                        break;
                    case "Update user profile":
                        // intent for update user profile activity
                        this.openUpdateUser();
                        break;
                    case "Parking app manual":
                        // intent for parking app manual fragment
                        openManualFragment();
                        break;
                    case "Customer support":
                        // intent for customer support fragment
                        openSupportFragment();
                        break;
                    case "Preferences":
                        // intent to preferences page
                        openPrefsActivity();
                        break;
                    case "Sign out":
                        // sign out operation
                        changeToSignOutActivity();
                        break;


                }
           }

           private void openParkingReceipt() {
               Intent receiptIntent = new Intent(HomepageActivity.this, ParkingReceipt.class);
               startActivity(receiptIntent);
               finish();
           }


           private void openParkingReceiptList() {
               Intent receiptListIntent = new Intent(HomepageActivity.this, ParkingReceiptList.class);
               startActivity(receiptListIntent);
               finish();
           }

           private void openUpdateUser() {
               Intent updateIntent = new Intent(HomepageActivity.this, UpdateUserActivity.class);
               updateIntent.putExtra("EXTRA_EMAIL", email);
               startActivity(updateIntent);
           }
       });

    }

    private void openPrefsActivity(){
        Intent prefsIntent = new Intent(HomepageActivity.this, PreferencesActivity.class);
        startActivity(prefsIntent);
    }

    private void openMapsActivity(){
        Intent mapsIntent = new Intent(HomepageActivity.this, MapsActivity.class);
        startActivity(mapsIntent);
    }

    private void openSupportFragment() {

        SupportFragment supportFragment = new SupportFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_container, supportFragment, "SUPPORT_FRAGMENT").commit();

    }

    private void openManualFragment() {

        AppManualFragment appManualFragment = new AppManualFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_container, appManualFragment, "APP_MANUAL_FRAGMENT").commit();

    }

    private void changeToSignOutActivity(){
        Intent signOutIntent = new Intent(HomepageActivity.this, SignInActivity.class);
        startActivity(signOutIntent);
        finishAffinity();
    }

}
