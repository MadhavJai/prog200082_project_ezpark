package com.example.ezpark2019.viewmodel;

import android.app.Application;

import com.example.ezpark2019.db.UserRepository;
import com.example.ezpark2019.model.User;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

/**
 * EzPark2019 created by tyrellst.kitts
 * student ID : your_student_id
 * on 2019-11-05
 */

public class UserViewModel extends AndroidViewModel {

    private LiveData<List<User>> allUsers;
    private UserRepository userRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        allUsers = userRepository.getAllUsers();
    }

    public void insert(User user){
        userRepository.insert(user);
    }

    public void update(User user) {
        userRepository.update(user);
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }
}
