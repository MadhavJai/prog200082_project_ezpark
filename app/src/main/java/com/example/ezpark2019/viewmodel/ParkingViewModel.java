package com.example.ezpark2019.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.ezpark2019.db.ParkingRepository;
import com.example.ezpark2019.model.Parking;

import java.util.List;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-05
 */
public class ParkingViewModel extends AndroidViewModel {

    private LiveData<List<Parking>> allParking;
    private ParkingRepository parkingRepository;

    public ParkingViewModel(@NonNull Application application) {
        super(application);
        parkingRepository = new ParkingRepository(application);
        allParking = parkingRepository.getAllParkings();
    }

    public void insert(Parking parking){

        parkingRepository.insert(parking);
    }

    public LiveData<List<Parking>> getAllParkings() {
        return allParking;
    }

}
