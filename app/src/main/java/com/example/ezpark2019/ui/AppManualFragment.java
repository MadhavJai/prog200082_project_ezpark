package com.example.ezpark2019.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ezpark2019.HomepageActivity;
import com.example.ezpark2019.R;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-09
 */
public class AppManualFragment extends Fragment {

    Button btnCloseManualFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_app_manual, container, false);

        btnCloseManualFragment = root.findViewById(R.id.btnCloseManualFragment);
        btnCloseManualFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HomepageActivity.class);
                startActivity(intent);
            }
        });

        final WebView mWebView = root.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);

        //to open web page in app fragment and not in browser
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Log.e("AppManualFragment", error.getDescription().toString());
            }
        });

        mWebView.loadUrl("file:///android_asset/html/app_manual.html");


        return root;

    }



}


