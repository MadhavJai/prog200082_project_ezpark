package com.example.ezpark2019.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.ezpark2019.HomepageActivity;
import com.example.ezpark2019.R;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-09
 */
public class SupportFragment extends Fragment implements View.OnClickListener {

    Button btnCall;
    Button btnEmail;
    Button btnCloseSuppFragment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_support, container, false);

        btnCall = root.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(this);

        btnEmail = root.findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(this);

        btnCloseSuppFragment = root.findViewById(R.id.btnCloseSuppFragment);
        btnCloseSuppFragment.setOnClickListener(this);

        return root;


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCall:
                this.makeCall();
                break;
            case R.id.btnEmail:
                this.makeEmail();
                break;
            case R.id.btnCloseSuppFragment:
                Intent intent = new Intent(getActivity(), HomepageActivity.class);
                startActivity(intent);
                break;
        }
    }

    //send call using intent service
    private void makeCall() {
        //create an intent service to perform call operation
        Intent callIntent = new Intent(Intent.ACTION_CALL);

        //which number to call
        callIntent.setData(Uri.parse("tel:16479747780"));

        if(ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // if permission not granted
            Log.e("SupportFragment", "Call permission not granted");
            return;
        }

        startActivity(callIntent);

    }

    //send email using intent service
    private void  makeEmail() {

        // prebuilt intent  for email
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);

        //set the type of content in email
        emailIntent.setType("text/plain");

        // receiver address
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"EzParkSupport@EzPark.com"});

        //subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "This is a text message... Send it to your own email address");

        emailIntent.setType("message/rfc822");

        startActivity(Intent.createChooser(emailIntent, "Please select email client"));
    }

}
