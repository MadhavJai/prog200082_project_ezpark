package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.room.Update;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ezpark2019.model.User;
import com.example.ezpark2019.viewmodel.UserViewModel;

import java.util.List;

public class UpdateUserActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtName;
    EditText edtEmail;
    EditText edtPhone;
    EditText edtLicense;
    EditText edtPassword;
    EditText edtConfirm;
    EditText edtCreditNumber;
    EditText edtCreditName;
    EditText edtCreditExpiryMonth;
    EditText edtCreditExpiryYear;
    EditText edtCreditCvvNumber;

    Button btnCancel;
    Button btnSubmit;

    String name;
    String email;
    String UEmail;
    String phoneNumber;
    String license;
    String password;
    String creditNumberString;
    long creditNumber;
    String creditName;
    String creditExpiryMonthString;
    int creditExpiryMonth;
    String creditExpiryYearString;
    int creditExpiryYear;
    String creditCvvNumberString;
    int creditCvvNumber;
    int testMonth;
    int testYear;

    UserViewModel userViewModel;
    User user;

    public static final String USER_PREF = "com.example.ezpark2019.userpref";
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        email = this.getIntent().getStringExtra("EXTRA_EMAIL");

        this.referWidgets();

        userViewModel = new UserViewModel(getApplication());
        userViewModel.getAllUsers().observe(UpdateUserActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                for (User user: users){
                    if(email.equals(user.getEmail())) {
                        name = user.getName();
                        phoneNumber = user.getPhoneNumber();
                        license = user.getCarLicenseInformation();
                        password = user.getPassword();
                        creditNumber = user.getCreditCardNum();
                        creditName = user.getCardholder();
                        creditExpiryMonth = user.getMonth();
                        creditExpiryYear = user.getYear();
                        creditCvvNumber = user.getCvvNumber();

                        edtName.setText(name);
                        edtEmail.setText(email);
                        edtPhone.setText(phoneNumber);
                        edtLicense.setText(license);
                        edtPassword.setText(password);
                        creditNumberString = String.valueOf(creditNumber);
                        edtCreditNumber.setText(creditNumberString);
                        edtCreditName.setText(creditName);
                        creditExpiryYearString = String.valueOf(creditExpiryYear);
                        edtCreditExpiryYear.setText(creditExpiryYearString);
                        creditExpiryMonthString = String.valueOf(creditExpiryMonth);
                        edtCreditExpiryMonth.setText(creditExpiryMonthString);
                        creditCvvNumberString = String.valueOf(creditCvvNumber);
                        edtCreditCvvNumber.setText(creditCvvNumberString);
                    }
                }
            }
        });
    }

    private void referWidgets(){
        edtName = findViewById(R.id.edtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPhone = findViewById(R.id.edtPhone);
        edtLicense = findViewById(R.id.edtLicense);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirm = findViewById(R.id.edtConfirm);
        edtCreditNumber = findViewById(R.id.edtCreditNumber);
        edtCreditName = findViewById(R.id.edtCreditName);
        edtCreditExpiryYear = findViewById(R.id.edtCreditExpiryYear);
        edtCreditExpiryMonth = findViewById(R.id.edtCreditExpiryMonth);
        edtCreditCvvNumber = findViewById(R.id.edtCreditCvvNumber);

        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancel:
                this.returnToHome();
                break;
            case R.id.btnSubmit:
                if (this.validateData()){
                    this.updateUser();
                    this.returnToHome();
                }
                break;
        }
    }

    void returnToHome() {
        Intent HomeIntent = new Intent(UpdateUserActivity.this, HomepageActivity.class);
        startActivity(HomeIntent);
    }

    private boolean validateData(){
        boolean allValidations = true;

        //Checking if string is empty or not and that it only contains letters
        if (edtName.getText().toString().isEmpty()){
            edtName.setError("Please enter name");
            allValidations = false;
        }

        //Checking is string is empty and if email address is valid
        if (edtEmail.getText().toString().isEmpty()){
            edtEmail.setError("Please enter email");
            allValidations = false;
        }else if (!Utils.isValidEmail(edtEmail.getText().toString())){
            edtEmail.setError("Please enter valid email address");
            allValidations = false;
        }

        //Check if string is empty, if string contains only numbers and if it has the right amount of digits
        if (edtPhone.getText().toString().isEmpty()){
            edtPhone.setError("Please enter phone number (no spaces)");
            allValidations = false;
        } else if(!edtPhone.getText().toString().matches("[0-9]+")) {
            edtPhone.setError("Phone number must only contain numbers (no spaces)");
            allValidations = false;
        } else if(edtPhone.getText().toString().length() < 10 || edtPhone.getText().toString().length() > 11) {
            edtPhone.setError("Phone number must be 10 or 11 digits");
            allValidations = false;
        }

        //Checking if string is empty and if it has the right amount of characters
        if (edtLicense.getText().toString().isEmpty()){
            edtLicense.setError("Please enter license plate number");
            allValidations = false;
        } else if(edtLicense.getText().toString().length() < 2 ||edtLicense.getText().toString().length() > 8) {
            edtLicense.setError("License plate number must be between 2 and 8 characters");
            allValidations = false;
        }

        //Checking if string is empty
        if (edtPassword.getText().toString().isEmpty()){
            edtPassword.setError("Please enter password");
            allValidations = false;
        }

        //Checking if string is empty and if it matches prior password string
        if (edtConfirm.getText().toString().isEmpty()){
            edtConfirm.setError("Please enter password");
            allValidations = false;
        }else if (!edtPassword.getText().toString().equals(edtConfirm.getText().toString())){
            edtConfirm.setError("Both passwords must be same");
            allValidations = false;
        }

        //Checking if string is empty, if it only contains numbers and if it's equal to 16 digits
        if (edtCreditNumber.getText().toString().isEmpty()){
            edtCreditNumber.setError("Please enter card number (no spaces)");
            allValidations = false;
        }  else if(!edtCreditNumber.getText().toString().matches("[0-9]+")) {
            edtCreditNumber.setError("Card number must only contain numbers (no spaces)");
            allValidations = false;
        } else if(edtCreditNumber.getText().toString().length() != 16) {
            edtCreditNumber.setError("Card number must be 16 digits long (no spaces)");
            allValidations = false;
        }

        //Checking if string is empty and that it only contains letters
        if (edtCreditName.getText().toString().isEmpty()){
            edtCreditName.setError("Please enter cardholder name");
            allValidations = false;
        }

        //Checking if string is empty, if it only contains number and if it's between 1 and 12
        if (!edtCreditExpiryMonth.getText().toString().isEmpty()){
            testMonth = Integer.parseInt(edtCreditExpiryMonth.getText().toString());
        }
        if (edtCreditExpiryMonth.getText().toString().isEmpty()){
            edtCreditExpiryMonth.setError("Please enter card expiry month");
            allValidations = false;
        } else if(!edtCreditExpiryMonth.getText().toString().matches("[0-9]+")) {
            edtCreditExpiryMonth.setError("Must be a number");
            allValidations = false;
        } else if(testMonth < 1 || testMonth > 12) {
            edtCreditExpiryMonth.setError("Must be between 1 and 13");
            allValidations = false;
        }

        //Checking if string is empty, if it only contains numbers and if it's between 0 and 99
        if (!edtCreditExpiryYear.getText().toString().isEmpty()){
            testYear = Integer.parseInt(edtCreditExpiryYear.getText().toString());
        }
        if (edtCreditExpiryYear.getText().toString().isEmpty()){
            edtCreditExpiryYear.setError("Please enter card expiry year (last two digits)");
            allValidations = false;
        } else if(!edtCreditExpiryYear.getText().toString().matches("[0-9]+")) {
            edtCreditExpiryYear.setError("Must be a number");
            allValidations = false;
        } else if(testYear < 0 || testYear > 99) {
            edtCreditExpiryYear.setError("Must be between 00 and 100");
            allValidations = false;
        }

        //Checking if string is empty, if it only contains numbers and if it's only 3 digits long
        if (edtCreditCvvNumber.getText().toString().isEmpty()){
            edtCreditCvvNumber.setError("Please enter CVV number");
            allValidations = false;
        } else if(!edtCreditCvvNumber.getText().toString().matches("[0-9]+")) {
            edtCreditCvvNumber.setError("Must be a number");
            allValidations = false;
        } else if(edtCreditCvvNumber.getText().toString().length() != 3) {
            edtCreditCvvNumber.setError("Must be between 3 digits");
            allValidations = false;
        }

        return allValidations;
    }

    private void updateUser() {

        name = edtName.getText().toString();
        UEmail = edtEmail.getText().toString();
        phoneNumber = edtPhone.getText().toString();
        license = edtLicense.getText().toString();
        password = edtPassword.getText().toString();
        creditNumberString = edtCreditNumber.getText().toString();
        creditNumber = Long.parseLong(creditNumberString);
        creditName = edtCreditName.getText().toString();
        creditExpiryMonthString = edtCreditExpiryMonth.getText().toString();
        creditExpiryMonth = Integer.parseInt(creditExpiryMonthString);
        creditExpiryYearString = edtCreditExpiryYear.getText().toString();
        creditExpiryYear = Integer.parseInt(creditExpiryYearString);
        creditCvvNumberString = edtCreditCvvNumber.getText().toString();
        creditCvvNumber = Integer.parseInt(creditCvvNumberString);

        userViewModel.getAllUsers().observe(UpdateUserActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                for (User user: users){
                    if(email.equals(user.getEmail())) {
                        user.setName(name);
                        user.setEmail(UEmail);
                        user.setPhoneNumber(phoneNumber);
                        user.setCarLicenseInformation(license);
                        user.setPassword(password);
                        user.setCreditCardNum(creditNumber);
                        user.setCardholder(creditName);
                        user.setMonth(creditExpiryMonth);
                        user.setYear(creditExpiryYear);
                        user.setCvvNumber(creditCvvNumber);

                        userViewModel.update(user);
                        Log.d("SignUpActivity", user.toString());
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent homepageIntent  = new Intent(UpdateUserActivity.this, HomepageActivity.class);
        startActivity(homepageIntent);
        finishAffinity();
        super.onBackPressed();
    }
}
