package com.example.ezpark2019;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ezpark2019.model.User;
import com.example.ezpark2019.viewmodel.UserViewModel;

import java.util.List;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtEmail;
    EditText edtPassword;
    Button btnSignIn;
    TextView txtSignUp;
    Switch swtRemember;
    TextView txtForgetPassword;


    String email = "";
    String password = "";

    public boolean passwordReset = false;
    public int isValidUserFound = 0; //set to 1 if valid email was found , set to 2 if no valid email was found.
    public User foundUser;

    public static final int SIGN_UP_REQUEST_CODE = 1;
    public static final String USER_PREF = "com.example.ezpark2019.userpref";
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";

    UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        this.referWidgets();

        userViewModel = new UserViewModel(getApplication());

        userViewModel.getAllUsers().observe(SignInActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                //Task when the data changes
                for (User user : users) {
                    Log.e("SignInActivity", user.toString());
                }
            }
        });

        this.getRememberedData();
    }

    void referWidgets(){
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(this);

        txtSignUp = findViewById(R.id.txtSignUp);
        txtSignUp.setOnClickListener(this);

        swtRemember = findViewById(R.id.swtRemember);
        txtForgetPassword = findViewById(R.id.txtForgotPassword);
        txtForgetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignIn:
                this.signIn();
                break;
            case R.id.txtSignUp:
                this.signUp();
                break;
            case R.id.txtForgotPassword:
                this.forgotPassword();
                break;
        }
    }

    void signIn(){
        email = edtEmail.getText().toString();
        password = edtPassword.getText().toString();

        if (this.authenticateUser(email, password)) {

            if (swtRemember.isChecked()) {
                this.rememberData();
            }
            else {
                this.forgetData();
            }
            Toast.makeText(this, "Login successful",Toast.LENGTH_LONG).show();

            Intent homeIntent = new Intent(SignInActivity.this, HomepageActivity.class);
            homeIntent.putExtra("EXTRA_EMAIL", email);
            startActivity(homeIntent);
        }else{
            Toast.makeText(this, "Incorrect Username/Password! Try again.",Toast.LENGTH_LONG).show();
        }
    }

    void signUp(){
        Intent signUpIntent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivityForResult(signUpIntent, SIGN_UP_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_UP_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                User newUser = (User) data.getSerializableExtra("com.example.ezpark2019.REPLY");
                Log.d("SIGNINACTIVITY", newUser.toString());

                userViewModel.insert(newUser);
            }
        }
    }

    private boolean authenticateUser(String email, String password) {

        List<User> allUsers = userViewModel.getAllUsers().getValue();

        for (User user : allUsers) {
            if (user.getEmail().equals(email) && user.getPassword().equals((password))) {
                return true;
            }
        }
        return false;
    }

    private void rememberData() {
        SharedPreferences sp = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);

        sp.edit().putString(EMAIL, edtEmail.getText().toString()).commit();
        sp.edit().putString(PASSWORD, edtPassword.getText().toString()).commit();
    }

    private void forgetData() {
        SharedPreferences sp = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        sp.edit().clear().commit();

        sp.edit().remove(PASSWORD).commit();
    }

    private void getRememberedData() {
        SharedPreferences sp = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);

        edtEmail.setText(sp.getString(EMAIL, ""));
        edtPassword.setText(sp.getString(PASSWORD, ""));
    }

    private void forgotPassword() {

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.reset_password, null);


        AlertDialog resetDialog = new AlertDialog.Builder(this).setTitle("Reset password").setMessage("Would you like to reset your password?").setView(dialogView)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText edtInputEmail = dialogView.findViewById(R.id.edtInputEmail);
                        EditText edtNewPassword = dialogView.findViewById(R.id.edtNewPassword);
                        EditText edtConfirm = dialogView.findViewById(R.id.edtConfirm);

                        final String inputEmail = edtInputEmail.getText().toString();
                        final String newPassword = edtNewPassword.getText().toString();

                        String confirm = edtConfirm.getText().toString();
                        if(newPassword.equals(confirm)) {
                            userViewModel.getAllUsers().observe(SignInActivity.this, new Observer<List<User>>() {
                                @Override
                                public void onChanged(List<User> users) {
                                    for (User user : users) {
                                        if (inputEmail.equals(user.getEmail())) {
                                            passwordReset = true;
                                            foundUser =  user;
                                            isValidUserFound = 1;
                                        }
                                        else{
                                            isValidUserFound = 2;
                                        }
                                        break;
                                    }
                                }
                            });
                        }
                        else {
                            Toast.makeText(SignInActivity.this, "Passwords do not match", Toast.LENGTH_LONG).show();
                        }

                        if(passwordReset==true){
                            changePassword(newPassword, foundUser);
                            Toast.makeText(SignInActivity.this, "Password changed!", Toast.LENGTH_LONG).show();
                        }
                        else if(isValidUserFound==2){
                            Toast.makeText(SignInActivity.this, "No account registered to that email", Toast.LENGTH_LONG).show();
                        }
                    }
                }).setNegativeButton("No", null).create();




        resetDialog.show();
    }

    private void changePassword(String newPassword, User userObject){
        userObject.setPassword(newPassword);
        userViewModel.update(userObject);
    }
}





























