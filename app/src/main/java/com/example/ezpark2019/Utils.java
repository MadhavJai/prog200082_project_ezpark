package com.example.ezpark2019;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * EzPark2019 created by tyrellst.kitts
 * student ID : your_student_id
 * on 2019-11-07
 */

public class Utils {
    public static boolean isValidEmail(String target){
        return (!TextUtils.isEmpty(target) &&
                Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
