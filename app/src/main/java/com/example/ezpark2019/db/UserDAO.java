package com.example.ezpark2019.db;

import com.example.ezpark2019.model.User;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

/**
 * EzPark2019 created by mvinzon
 * student ID : 991518415
 * on 2019-11-05
 */
@Dao
public interface UserDAO {

    @Insert
    void insert(User user);

    @Update
    void update(User user);

    @Query("SELECT * FROM user_table ORDER BY user_id ASC")
    LiveData<List<User>> getAllUsers();
}
