package com.example.ezpark2019.db;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.ezpark2019.model.Parking;

import java.util.List;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-05
 */
public class ParkingRepository {

    private ParkingDAO parkingDao;
    private LiveData<List<Parking>> allParkings;

    public ParkingRepository(Application application) {
        ParkingDB parkingDB = ParkingDB.getINSTANCE(application);
        parkingDao = parkingDB.parkingDAO();
        allParkings = parkingDao.getAllParking();
    }

    public LiveData<List<Parking>> getAllParkings() {

        return allParkings;

    }

    public void insert(Parking parking){

        new ParkingRepository.insertAsyncTask(parkingDao).execute(parking);

    }

    private static class insertAsyncTask extends AsyncTask<Parking, Void, Void> {

        private ParkingDAO asyncTaskDao;

        insertAsyncTask(ParkingDAO parkingDao) {
            asyncTaskDao = parkingDao;
        }

        @Override
        protected Void doInBackground( final Parking... parkings) {
            asyncTaskDao.insert(parkings[0]);
            return null;
        }
    }

}
