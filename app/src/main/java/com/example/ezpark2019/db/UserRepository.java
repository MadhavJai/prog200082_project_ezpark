package com.example.ezpark2019.db;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.ezpark2019.model.User;

import java.util.List;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-05
 */
public class  UserRepository {

    private UserDAO userDao;
    private LiveData<List<User>> allUsers;

    public UserRepository(Application application) {
        UserDB userDB = UserDB.getInstance(application);
        userDao = userDB.userDAO();
        allUsers = userDao.getAllUsers();
    }

    public LiveData<List<User>> getAllUsers() {

        return allUsers;

    }

    public void insert(User user){

        new insertAsyncTask(userDao).execute(user);

    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDAO asyncTaskDao;

        insertAsyncTask(UserDAO userDao) {
            asyncTaskDao = userDao;
        }

        @Override
        protected Void doInBackground( final User... users) {
            asyncTaskDao.insert(users[0]);
            return null;
        }
    }


    public void update(User user) {
        new updateAsyncTask(userDao).execute(user);
    }

    private static class updateAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDAO asyncTaskDao;

        updateAsyncTask(UserDAO userDao) {asyncTaskDao = userDao; }


        @Override
        protected Void doInBackground(User... users) {
            asyncTaskDao.update(users[0]);
            return null;
        }
    }




}
