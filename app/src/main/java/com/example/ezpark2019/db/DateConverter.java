package com.example.ezpark2019.db;

import java.util.Date;

import androidx.room.TypeConverter;

/**
 * EzPark2019 created by tyrellst.kitts
 * student ID : your_student_id
 * on 2019-11-05
 */

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long dateLong) {
        return dateLong == null ? null : new Date(dateLong);
    }

    @TypeConverter
    public static Long fromDate(Date date){
        return date == null ? null : date.getTime();
    }
}
