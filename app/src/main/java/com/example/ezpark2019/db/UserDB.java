package com.example.ezpark2019.db;

import android.content.Context;
import android.service.autofill.UserData;

import com.example.ezpark2019.model.User;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

/**
 * EzPark2019 created by mvinzon
 * student ID : 991518415
 * on 2019-11-05
 */
//User Database
@Database(entities =  {User.class}, version = 1, exportSchema = false)
public abstract class UserDB extends RoomDatabase {

    public abstract UserDAO userDAO();

    private static volatile UserDB INSTANCE;

    public static UserDB getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (UserDB.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        UserDB.class, "ezpark_user_database")
                        .fallbackToDestructiveMigration()
                        .build();
            }
        }
        return INSTANCE;
    }
}
