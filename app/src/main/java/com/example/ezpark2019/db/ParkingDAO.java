package com.example.ezpark2019.db;

import com.example.ezpark2019.model.Parking;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

/**
 * EzPark2019 created by mvinzon
 * student ID : 991518415
 * on 2019-11-05
 */

@Dao
public interface ParkingDAO {

    @Insert
    void insert(Parking parking);

    @Query("SELECT * FROM parking_table ORDER BY parking_id ASC")
    LiveData<List<Parking>> getAllParking();
}
