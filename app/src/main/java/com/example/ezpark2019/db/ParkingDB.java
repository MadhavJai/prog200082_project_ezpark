package com.example.ezpark2019.db;

import android.content.Context;

import com.example.ezpark2019.model.Parking;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

/**
 * EzPark2019 created by tyrellst.kitts
 * student ID : your_student_id
 * on 2019-11-05
 */

@Database(entities = {Parking.class}, version = 1)
@TypeConverters(DateConverter.class)
public abstract class ParkingDB extends RoomDatabase {

    public abstract ParkingDAO parkingDAO();

    private static volatile ParkingDB INSTANCE;

    public static ParkingDB getINSTANCE(final Context context) {
        if(INSTANCE == null) {
            synchronized(ParkingDB.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ParkingDB.class, "ezpark_parking_database").build();
            }
        }
        return INSTANCE;
    }
}
