package com.example.ezpark2019.model;

import java.io.Serializable;
import java.util.Date;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-05
 */

@Entity(tableName = "parking_table")
public class Parking implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="parking_id")
    private int id;
    @ColumnInfo(name="license_plate")
    private String licensePlate;
    @ColumnInfo(name="date_time")
    private Date dateTime;
    @ColumnInfo(name="duration")
    private String duration;
    @ColumnInfo(name="building_code")
    private String buildingCode;
    @ColumnInfo(name="suit_number")
    private String suitNumber;
    @ColumnInfo(name="cost")
    private float cost;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public String getSuitNumber() {
        return suitNumber;
    }

    public void setSuitNumber(String suitNumber) {
        this.suitNumber = suitNumber;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public Parking(String licensePlate, Date dateTime, String duration, String buildingCode, String suitNumber, float cost) {
        this.licensePlate = licensePlate;
        this.dateTime = dateTime;
        this.duration = duration;
        this.buildingCode = buildingCode;
        this.suitNumber = suitNumber;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "User{" +
                "license Plate: " + licensePlate + '\'' +
                ", date-time: " + dateTime + '\'' +
                ", duration: " + duration + '\'' +
                ", building code: " + buildingCode + '\'' +
                ", suit #: " + suitNumber + '\'' +
                ", cost: " + cost +
                '}';
    }
}
