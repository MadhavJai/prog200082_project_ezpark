package com.example.ezpark2019.model;

/**
 * EzPark2019 createdby madhavj
 * studentID : 991522670
 * on 2019-11-05
 */

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "user_table")
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    private int userId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "email")
    private String email;
    @ColumnInfo(name = "password")
    private String password;
    @ColumnInfo(name = "phone_number")
    private String phoneNumber;
    @ColumnInfo(name = "car_license")
    private String carLicenseInformation;
    @ColumnInfo(name = "credit_card_num")
    private long creditCardNum;
    @ColumnInfo(name = "expiry_month")
    private int month;
    @ColumnInfo(name = "expiry_year")
    private int year;
    @ColumnInfo(name = "cardholder_name")
    private String cardholder;
    @ColumnInfo(name = "cvv_number")
    private int cvvNumber;

    public User( String name, String email, String phoneNumber, String carLicenseInformation, String password, long creditCardNum, String cardholder, int month, int year, int cvvNumber) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.carLicenseInformation = carLicenseInformation;
        this.creditCardNum = creditCardNum;
        this.month = month;
        this.year = year;
        this.cardholder = cardholder;
        this.cvvNumber = cvvNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCarLicenseInformation() {
        return carLicenseInformation;
    }

    public void setCarLicenseInformation(String carLicenseInformation) {
        this.carLicenseInformation = carLicenseInformation;
    }

    public long getCreditCardNum() {
        return creditCardNum;
    }

    public void setCreditCardNum(long creditCardNum) {
        this.creditCardNum = creditCardNum;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public int getCvvNumber() {
        return cvvNumber;
    }

    public void setCvvNumber(int cvvNumber) {
        this.cvvNumber = cvvNumber;
    }

    @Override
    public String toString(){

        return "User{ " + "Name: " + name + ", Email: " + email + ", Phone number: " + phoneNumber + ", Car license number: " + carLicenseInformation + ", Password: " + password  + ", Credit card number: " + creditCardNum + ", Name of cardholder: " + cardholder + ", Month/Year (expiry): " + month + "/" + year + ", CVV Number: " + cvvNumber + "}";
    }

}
