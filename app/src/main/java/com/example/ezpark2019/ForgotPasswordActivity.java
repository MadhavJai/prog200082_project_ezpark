package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ezpark2019.model.User;
import com.example.ezpark2019.viewmodel.UserViewModel;

import java.util.List;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtEmail;
    EditText edtPassword;
    EditText edtConfirm;
    Button btnCancel;
    Button btnSubmit;
    String email;
    String newPassword;

    UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        edtEmail = findViewById(R.id.edtEmail);
        edtPassword =findViewById(R.id.edtPassword);
        edtConfirm =findViewById(R.id.edtConfirm);
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancel:
                this.returnToSignIn();
                break;
            case R.id.btnSubmit:
                if (this.validateData()){
                    this.reset();
                    this.returnToSignIn();
                }
                break;
        }
    }

    void returnToSignIn() {
        Intent SignInIntent = new Intent(ForgotPasswordActivity.this, SignInActivity.class);
        startActivity(SignInIntent);
    }

    private boolean validateData(){
        boolean allValidations = true;

        //Checking is string is empty and if email address is valid
        if (edtEmail.getText().toString().isEmpty()){
            edtEmail.setError("Please enter email");
            allValidations = false;
        }else if (!Utils.isValidEmail(edtEmail.getText().toString())){
            edtEmail.setError("Please enter valid email address");
            allValidations = false;
        }

        //Checking if string is empty
        if (edtPassword.getText().toString().isEmpty()){
            edtPassword.setError("Please enter password");
            allValidations = false;
        }

        //Checking if string is empty and if it matches prior password string
        if (edtConfirm.getText().toString().isEmpty()){
            edtConfirm.setError("Please enter password");
            allValidations = false;
        }else if (!edtPassword.getText().toString().equals(edtConfirm.getText().toString())){
            edtConfirm.setError("Both passwords must be same");
            allValidations = false;
        }

        return allValidations;
    }

    private void reset() {
        email = edtEmail.getText().toString();
        newPassword = edtPassword.getText().toString();
        userViewModel.getAllUsers().observe(ForgotPasswordActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                for (User user: users){
                    if(email.equals(user.getEmail())) {
                        user.setPassword(newPassword);

                        userViewModel.update(user);
                        //Log.d("SignUpActivity", user.toString());
                    }
                }
            }
        });
        Toast.makeText(ForgotPasswordActivity.this, "Password reset", Toast.LENGTH_SHORT).show();
    }
}


//LayoutInflater inflater = getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.forgot_password, null);
//
//        AlertDialog resetDialog = new AlertDialog.Builder(this).setTitle("Reset password").setMessage("Would you like to reset your password?").setView(dialogView)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        userViewModel.getAllUsers().observe(SignInActivity.this, new Observer<List<User>>() {
//                            @Override
//                            public void onChanged(List<User> users) {
//                                for (User user: users){
//                                    if(inputEmail.equals(user.getEmail())) {
//                                        user.setPassword(newPassword);
//
//                                        userViewModel.update(user);
//                                    }
//                                }
//                            }
//                        });
//                    }
//                }).setNegativeButton("No", null).create();
//
//        resetDialog.show();












