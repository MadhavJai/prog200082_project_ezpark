package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

public class PreferencesActivity extends AppCompatActivity {

    Switch darkSwitch = null;
    Boolean drkSwitchState;
    View prefActView;
    View prefActViewRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        darkSwitch = findViewById(R.id.drkModeSwitch);
        prefActView = findViewById(R.id.prefActBackground);
        //prefActViewRoot = prefActView.getRootView();

        darkSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    drkSwitchState = true;
                    prefActView.setBackgroundColor(ContextCompat.getColor(PreferencesActivity.this, R.color.Black));
                    darkSwitch.setTextColor(ContextCompat.getColor(PreferencesActivity.this, R.color.GhostWhite));
                }
                else{
                    drkSwitchState = false;
                    //prefActViewRoot.setBackgroundColor(getResources().getColor(android.R.color.dodgerBlue));
                    prefActView.setBackgroundColor(ContextCompat.getColor(PreferencesActivity.this, R.color.DodgerBlue));
                    darkSwitch.setTextColor(ContextCompat.getColor(PreferencesActivity.this, R.color.GhostWhite));
                }
            }
        });


    }
}
