package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.ezpark2019.model.Parking;
import com.example.ezpark2019.model.User;
import com.example.ezpark2019.viewmodel.ParkingViewModel;
import com.example.ezpark2019.viewmodel.UserViewModel;

//Matthew and Madhav
public class NewParkingActivity extends AppCompatActivity implements View.OnClickListener {

    String buildCode;
    RadioGroup parkHours;
    RadioButton rdbSelected;
    String hoursPark;
    String plateNum;
    String suitNum;

    Date currentTime = Calendar.getInstance().getTime();

    float cost;

    EditText edtBcode;
    EditText edtPnum;
    EditText edtSNum;

    Button btnCancel;
    Button btnSubmit;

//    User user;
    Parking parking = new Parking("", currentTime, "", "", "", 0);
    UserViewModel userViewModel;
    ParkingViewModel parkingViewModel;

    Date parkingDate;
    int monthlyParkingCount=0;

    //Date date = Calendar.getInstance().getTime();
    int month = currentTime.getMonth();
    int year = currentTime.getYear();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_parking);

        this.referwidgets();

        userViewModel = new UserViewModel(getApplication());
        userViewModel.getAllUsers().observe(NewParkingActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                for (User user: users) {
                    plateNum = user.getCarLicenseInformation();
                }
            }
        });

        parkingViewModel = new ParkingViewModel(getApplication());
        parkingViewModel.getAllParkings().observe(NewParkingActivity.this, new Observer<List<Parking>>() {
            @Override
            public void onChanged(List<Parking> parkings) {
                for (Parking parking1: parkings) {
                    parkingDate = parking1.getDateTime();
                    if(parkingDate.getMonth() == currentTime.getMonth() && parkingDate.getYear() == currentTime.getYear()) {
                        monthlyParkingCount+=1;

                    }
                }
            }

        });

        Log.e("NewParkingActivity", "Free parkings used: " + monthlyParkingCount);
    }

    private void referwidgets() {
        edtBcode = findViewById(R.id.edt_buildcode);
        parkHours = findViewById(R.id.rdg_hours);

        edtPnum = findViewById(R.id.edt_platenum);

        edtSNum = findViewById(R.id.edt_suitnum);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btnSubmit:
                this.getData();
                if (this.validateData(plateNum)) { //If care plate number is same as user profile
                    this.createParking();
                    //this.returnMain();
                }
                break;

            case R.id.btnCancel:
                this.returnMain();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent homepageIntent  = new Intent(NewParkingActivity.this, HomepageActivity.class);
        startActivity(homepageIntent);
        finishAffinity();
        super.onBackPressed();
    }




    private boolean validateData(String licensePlate) {
        List<User> allUsers = userViewModel.getAllUsers().getValue();

        for (User user: allUsers) {
            if (user.getCarLicenseInformation().equals(licensePlate)) {
                return true;
            }else{
                edtPnum.setError("Your license plate number does not match the one in the records >:(");
            }
        }
        return false;
    }

    private void getData(){

        plateNum = edtPnum.getText().toString();

        rdbSelected = findViewById(parkHours.getCheckedRadioButtonId());

        hoursPark = rdbSelected.getText().toString();

        buildCode = edtBcode.getText().toString();

        suitNum = edtSNum.getText().toString();

    }


    private void createParking() {

        if(monthlyParkingCount < 3){
            cost = 0;
        }
        else if(monthlyParkingCount >= 3){
            calculateOrder();
        }


        Parking parking = new Parking(plateNum, currentTime, hoursPark, buildCode, suitNum, cost);
        Log.e("NewParkingActivity", parking.toString());
        parkingViewModel.insert(parking);
        Toast.makeText(NewParkingActivity.this, "Parking added!", Toast.LENGTH_SHORT).show();

        // Intent to parking info activity where user can review the details of their newly ordered parking
        Intent parkingInfoIntent = new Intent(NewParkingActivity.this, ParkingInfo.class);
        parkingInfoIntent.putExtra("NEW_PARKING_ORDER", parking);
        startActivity(parkingInfoIntent);

    }

    private void calculateOrder(){

        switch (rdbSelected.getId()){

            case R.id.rdb_onehr:
                cost = 4;
                break;
            case R.id.rdb_threehr:
                cost = 8;
                break;
            case R.id.rdb_tenhr:
                cost = 12;
                break;
            case R.id.rdb_24hr:
                cost = 20;
                break;
        }

    }

    private void returnMain() {
        Intent intentObject = new Intent(NewParkingActivity.this, HomepageActivity.class);
        startActivity(intentObject);
        finish();
    }
}
