package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ezpark2019.model.Parking;

import java.util.Date;

public class ParkingInfo extends AppCompatActivity implements View.OnClickListener{


    TextView txtLicense;
    TextView txtDate;
    TextView txtDuration;
    TextView txtBuilding;
    TextView txtSuitNum;
    TextView txtCost;
    Parking receivedParking;
    Parking newParkingOrder;

    Button btnCloseSelectedParking;

    String license;
    Date date;
    String duration;
    String building;
    String suitNum;
    float cost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_info);

        this.referWidgets();

        Intent receivedIntent = getIntent();

        // if received intent from he receiptlist activity is null, it means the recevied intent is from the add parking activity
        if(receivedIntent.getSerializableExtra("PARKING_INFO") == null){
            newParkingOrder = (Parking) receivedIntent.getSerializableExtra("NEW_PARKING_ORDER");
            this.setParkingData(newParkingOrder);
            this.addToWidgets();
        }
        else{
            receivedParking = (Parking) receivedIntent.getSerializableExtra("PARKING_INFO");
            this.setParkingData(receivedParking);
            this.addToWidgets();
        }

    }




    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close_parking_info:
                // if the received intent hasnt been assigned to newParkingOrder Parking object, then that means the recieved intent must have been from receiptlist activity and not from add parking activity
                if(newParkingOrder==null){
                    Intent intentObject = new Intent(ParkingInfo.this, ParkingReceiptList.class);
                    startActivity(intentObject);
                    finishAffinity();
                }
                else{
                    Intent intentObject = new Intent(ParkingInfo.this, HomepageActivity.class);
                    startActivity(intentObject);
                    finishAffinity();
                }

        }
    }


    private void setParkingData(Parking parkingObject){

        license = parkingObject.getLicensePlate();
        date = parkingObject.getDateTime();
        duration = parkingObject.getDuration();
        building = parkingObject.getBuildingCode();
        suitNum =  parkingObject.getSuitNumber();
        cost = parkingObject.getCost();

        Log.e("ParkingInfo", parkingObject.toString());
        Log.e("parkingInfo", parkingObject.getSuitNumber());
    }

    private void referWidgets(){
        txtLicense = findViewById(R.id.tv_license_val2);
        txtDate = findViewById(R.id.tv_dt_val2);
        txtDuration = findViewById(R.id.tv_duration_val2);
        txtBuilding = findViewById(R.id.tv_code_val2);
        txtSuitNum = findViewById(R.id.tv_suitnum_val2);
        txtCost = findViewById(R.id.tv_cost_val2);
        btnCloseSelectedParking = findViewById(R.id.btn_close_parking_info);
        btnCloseSelectedParking.setOnClickListener(this);
    }

    private void addToWidgets() {
        txtLicense.setText(license);
        txtDate.setText(date.toString());
        txtDuration.setText(duration);
        txtBuilding.setText(building);
        txtSuitNum.setText(suitNum);
        txtCost.setText("$" + cost);
    }
}
