package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ezpark2019.model.Parking;
import com.example.ezpark2019.viewmodel.ParkingViewModel;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

public class ParkingReceipt extends AppCompatActivity implements View.OnClickListener {

    TextView txtLicense;
    TextView txtDate;
    TextView txtDuration;
    TextView txtBuilding;
    TextView txtSuitNum;
    TextView txtCost;

    ParkingViewModel parkingViewModel;

    String license;
    Date date;
    String duration;
    String building;
    String suitNum;
    float cost;

    Button btnClose;

    Parking parking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_receipt);

        this.referWidgets();

        parkingViewModel = new ParkingViewModel(getApplication());
        parkingViewModel.getAllParkings().observe(ParkingReceipt.this, new Observer<List<Parking>>() {
            @Override
            public void onChanged(List<Parking> parkings) {
                for (Parking parking1: parkings) {
//                    Log.e("ParkingReceipt", parking.toString());
                    parking = parking1;
                    license = parking.getLicensePlate();
                    date = parking.getDateTime();
                    duration = parking.getDuration();
                    building = parking.getBuildingCode();
                    suitNum = parking.getSuitNumber();
                    cost = parking.getCost();

                    addToWidgets();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent homepageIntent  = new Intent(ParkingReceipt.this, HomepageActivity.class);
        startActivity(homepageIntent);
        finishAffinity();
        super.onBackPressed();
    }

    private void referWidgets() {
        txtLicense = findViewById(R.id.tv_license_val);
        txtDate = findViewById(R.id.tv_dt_val);
        txtDuration = findViewById(R.id.tv_duration_val);
        txtBuilding = findViewById(R.id.tv_code_val);
        txtSuitNum = findViewById(R.id.tv_suitnum_val);
        txtCost = findViewById(R.id.tv_cost_val);
        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

    }

    private void addToWidgets() {
        txtLicense.setText(license);
        txtDate.setText(date.toString());
        txtDuration.setText(duration);
        txtBuilding.setText(building);
        txtSuitNum.setText(suitNum);
        txtCost.setText("$" + cost);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                Intent intentObject = new Intent(ParkingReceipt.this, HomepageActivity.class);
                startActivity(intentObject);
                finish();
        }
    }
}
