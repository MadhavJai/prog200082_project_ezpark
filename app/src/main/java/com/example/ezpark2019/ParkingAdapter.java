package com.example.ezpark2019;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ezpark2019.model.Parking;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * EzPark2019 created by mvinzon
 * student ID : 991518415
 * on 2019-11-23
 */
public class ParkingAdapter extends ArrayAdapter {

    Context context;


    public ParkingAdapter(@NonNull Context context, ArrayList<Parking> parkings) {
        super(context, 0, parkings);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.parking_item, parent, false);
        }

        ImageView imgPark = convertView.findViewById(R.id.imgPark);
        TextView tvDetail = convertView.findViewById(R.id.tvDetail);

        Parking parking = (Parking) getItem(position);

        imgPark.setImageResource(R.drawable.ic_parking);
        tvDetail.setText(parking.getDateTime().toString());

        return convertView;

    }
}
