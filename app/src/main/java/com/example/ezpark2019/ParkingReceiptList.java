package com.example.ezpark2019;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ezpark2019.model.Parking;
import com.example.ezpark2019.viewmodel.ParkingViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParkingReceiptList extends AppCompatActivity implements View.OnClickListener{

    ListView lstParking;
    ArrayAdapter parkingAdapter;
    ArrayList<Parking> parkingArrayList;
    ParkingViewModel parkingViewModel;
    Parking chosenParking;

    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_receipt_list);

        this.populateParkingList();

        lstParking = findViewById(R.id.lst_parking);
        parkingAdapter = new ParkingAdapter(ParkingReceiptList.this, parkingArrayList);
        lstParking.setAdapter(parkingAdapter);

        parkingViewModel = new ParkingViewModel(getApplication());

        parkingViewModel.getAllParkings().observe(ParkingReceiptList.this, new Observer<List<Parking>>() {
            @Override
            public void onChanged(List<Parking> parkings) {
                parkingArrayList.clear();

                for (Parking parking : parkings) {
                    Log.e("ParkingReceiptList", parking.toString());
                    parkingArrayList.add(parking);
                }
                parkingAdapter.notifyDataSetChanged();
            }
        });

        this.referWidgets();
        this.listViewListener();
    }

    private void referWidgets() {
        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);
    }


    private void listViewListener() {
        lstParking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String buildingCode = parkingArrayList.get(position).getBuildingCode();
                String duration = parkingArrayList.get(position).getDuration();
                String license = parkingArrayList.get(position).getLicensePlate();
                String suitNum = parkingArrayList.get(position).getSuitNumber();
                Float cost = parkingArrayList.get(position).getCost();
                Date dateTime = parkingArrayList.get(position).getDateTime();

                chosenParking = new Parking(license, dateTime, duration, buildingCode, suitNum, cost);


                Intent parkingInfoIntent = new Intent(ParkingReceiptList.this, ParkingInfo.class);
                parkingInfoIntent.putExtra("PARKING_INFO", chosenParking);
                startActivity(parkingInfoIntent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent homepageIntent  = new Intent(ParkingReceiptList.this, HomepageActivity.class);
        startActivity(homepageIntent);
        finishAffinity();
        super.onBackPressed();
    }

    private void populateParkingList() {
        parkingArrayList = new ArrayList<Parking>();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                Intent intentObject = new Intent(ParkingReceiptList.this, HomepageActivity.class);
                startActivity(intentObject);
                finish();
        }
    }
}
